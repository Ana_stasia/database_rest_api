import uuid

from database import Base
from sqlalchemy import Column, Integer, String
from sqlalchemy.dialects.postgresql import UUID


class Kitten(Base):
    __tablename__ = "kittens"
    id = Column(
        UUID(as_uuid=True),
        primary_key=True,
        index=True,
        default=uuid.uuid4,
    )
    name = Column(String)
    age = Column(Integer)
    color = Column(String)
    fur = Column(String)
