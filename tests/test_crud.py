from unittest.mock import create_autospec

from sqlalchemy.orm import Session

from ..app.api import crud, models, schemas


def test_create_kitten_success():
    mock_session = create_autospec(Session, instance=True)
    mock_kitten_data = schemas.KittenCreate(
        name="Fluffy", age=2, color="white", fur="long"
    )

    result = crud.create_kitten(db=mock_session, kitten=mock_kitten_data)

    mock_session.add.assert_called_once()
    mock_session.commit.assert_called_once()
    mock_session.refresh.assert_called_once()
    assert isinstance(result, models.Kitten)
